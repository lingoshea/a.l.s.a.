#!/bin/bash

INITRAMFS="initramfs"

cd busybox
cp busybox-config.alsa.p100 .config

echo "Building ..."

if [ X"$NOBUILD" = X ]; then
  make -j "$(nproc)" > /dev/null
  make install > /dev/null
fi

# inside INITRAMFS
cd "$INITRAMFS"
mkdir -p dev lib/modules mnt proc root sys var tmp
sudo mknod ./dev/null     c 1 3
sudo mknod ./dev/console  c 5 1
sudo mknod ./dev/tty      c 5 0
sudo mknod ./dev/tty1     c 4 1
sudo mknod ./dev/tty2     c 4 2
sudo mknod ./dev/tty3     c 4 3
sudo mknod ./dev/tty4     c 4 4
sudo mknod ./dev/tty5     c 4 5
sudo mknod ./dev/tty6     c 4 6
sudo mknod ./dev/mem      c 1 1
sudo mknod ./dev/urandom  c 1 9
sudo mknod ./dev/random   c 1 8

sudo mknod ./dev/sda      b 8  0
sudo mknod ./dev/sda1     b 8  1
sudo mknod ./dev/sr0      b 11 0

#ln -s sbin/init init -f

sudo find . | cpio -H newc -o > ../initramfs.cpio
cd ..
cat initramfs.cpio | gzip > initramfs.igz

echo "$(file initramfs.igz) generated"
