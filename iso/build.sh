#!/bin/bash

# !! busybox & kernel should be generated now !!

cd syslinux
make -j "$(nproc)" > /dev/null
cd ..

CDROOT="/tmp/cdroot"

rm -rf $CDROOT

mkdir $CDROOT
cp ../utilities/busybox/initramfs.igz $CDROOT/initrd.igz
cp ../kernel/linux/arch/x86_64/boot/bzImage $CDROOT/linux
cp ../utilities/busybox/root.ext2 $CDROOT

mkdir $CDROOT/isolinux
cp ./syslinux/bios/core/isolinux.bin $CDROOT/isolinux
cp ./isolinux.cfg $CDROOT/isolinux
cp ./boot.txt $CDROOT/isolinux
cp ./syslinux/bios/com32/elflink/ldlinux/ldlinux.c32 $CDROOT/isolinux

ISONAME="alsa.$(date +"%Y-%m-%d").iso"

mkisofs -o $ISONAME -input-charset utf-8 -b isolinux/isolinux.bin -c isolinux/boot.cat \
    -no-emul-boot -boot-load-size 4 -boot-info-table $CDROOT
isohybrid $ISONAME

